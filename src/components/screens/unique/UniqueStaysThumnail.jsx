import React from "react";
import styled from "styled-components";
import Thumnails from "./Thumnails";
import Treehouse from "../../../asset/Treehouses.webp";
import Floathouse from "../../../asset/Floatinghouses.webp";
import Huts from "../../../asset/Huts.webp";
import Castles from "../../../asset/Castle.webp";
import Barns from "../../../asset/Barns.webp";
import Tinyhouse from "../../../asset/Tinyhouses.webp";
import FarmStay from "../../../asset/farmstays.webp";

const UniqueStaysThumnail = () => {
    const data = [
        {
            src: Treehouse,
            title: "Tree Houses",
        },
        {
            src: Floathouse,
            title: "Float Houses",
        },
        {
            src: Huts,
            title: "Huts",
        },
        {
            src: Castles,
            title: "Castles",
        },
        {
            src: Barns,
            title: "Barns",
        },
        {
            src: Tinyhouse,
            title: "Tiny Houses",
        },
        {
            src: FarmStay,
            title: "Farm stays",
        },
    ];
    return (
        <Container>
            <Top>Explore all types of unique stays</Top>
            <ThumnailContainer>
                {data.map((datas, index) => {
                    return <Thumnails src={datas.src} title={datas.title} />;
                })}
            </ThumnailContainer>
        </Container>
    );
};

export default UniqueStaysThumnail;
const Container = styled.div`
    height: 40vh;
    padding: 60px 100px;
`;
const Top = styled.h2`
    font-size: 24px;
    font-weight: 600;
    margin-bottom: 10px;
`;
const ThumnailContainer = styled.div`
    display: flex;
    justify-content: space-between;
`;
