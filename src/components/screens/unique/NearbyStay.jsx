import React from "react";
import styled from "styled-components";
import NearbyCard from "./NearbyCard";
import nearByCardDatas from "./nearByCardDatas";
const NearbyStay = () => {
    return (
        <NearbyContainer>
            <NearbyTop>
                <Title>Nearby stays in nature</Title>
                <Description>
                    Head outdoors for less crowded spaces with more room to
                    connect.
                </Description>
            </NearbyTop>
            <NearbyBottom>
                {nearByCardDatas.map((data, index) => {
                    return (
                        <NearbyCard
                            src={data.src}
                            rating={data.rating}
                            reviews={data.reviews}
                            title={data.title}
                            description={data.description}
                            price={data.price}
                        />
                    );
                })}
            </NearbyBottom>
            <Showall>
                <u>Show all nearby nature stays</u>
            </Showall>
        </NearbyContainer>
    );
};

export default NearbyStay;

const NearbyContainer = styled.div`
    // height: 0vh;
    padding: 60px 100px;
`;
const NearbyTop = styled.div`
    margin-bottom: 20px;
`;
const Title = styled.h2`
    font-size: 24px;
    font-weight: 600;
    // margin-bottom: 10px;
`;
const Description = styled.p``;
const NearbyBottom = styled.div`
    display: flex;
    // justify-contaent: flex-start;
    overflow-y: hidden;
    overflow-x: scroll;
    &::-webkit-scrollbar {
        display: none;
    }
`;
const Showall = styled.button`
    background: none;
    border: none;
    font-size: 15px;
    font-weight: 600;
    margin-top: 20px;
    cursor: pointer;
    padding: 10px;
    &:focus {
        outline: none;
    }
    &:hover {
        background-color: rgba(0, 0, 0, 0.04);
    }
`;
