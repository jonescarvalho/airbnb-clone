const NearbyData = [
    {
        src:
            "https://a0.muscache.com/im/pictures/46dc48b6-d28d-4343-9dbf-ce0024d1daa9.jpg?im_w=1200",
        rating: "4.7",
        reviews: 21,
        title: "Earth House- Munnar ",
        description: "The Mudhouse Marayoor - Cob (Nature Living)",
        price: "3,457",
    },
    {
        src:
            "https://a0.muscache.com/im/pictures/de6d045a-0680-4a6c-b551-e1ed623b23ca.jpg?im_w=1200",
        rating: "3.9",
        reviews: 98,
        title: "The MudHouse Marayoor",
        description: "The MudHouse Marayoor - Adobe (Nature Living)",
        price: "3,999",
    },
    {
        src:
            "https://a0.muscache.com/im/pictures/bd34cabb-4ca2-4bfc-b222-583b953888d4.jpg?im_w=1200",
        rating: "4.6",
        reviews: 238,
        title: "Vagamon Hue",
        description: "Farm stay hosted by Lucy",
        price: "7,200",
    },
    {
        src:
            "https://a0.muscache.com/im/pictures/bb36473b-4c84-4c40-9468-dc9be329580f.jpg?im_w=1200",
        rating: "5.0",
        reviews: 48,
        title: "Berigai Hill Home",
        description: "Berigai Hill Home. Great View, 2 beds, Food, Wifi",
        price: "3,999",
    },
    {
        src:
            "https://a0.muscache.com/im/pictures/9aaf8b2f-b276-4aca-9476-17e4b3681e01.jpg?im_w=1200",
        rating: "4.90",
        reviews: 118,
        title: "Mannoor Farm Stays",
        description: "Mannoor Farm Stays: Peaceful stay at the mountains",
        price: "2,499",
    },
    {
        src:
            "https://a0.muscache.com/im/pictures/15bee91a-3e3b-4827-8563-16799b339aaa.jpg?im_w=1200",
        rating: "4.80",
        reviews: 5,
        title: "Misty view Cottages",
        description: "Misty view Cottages on the hill. Coffee & Rain",
        price: "999",
    },
    {
        src:
            "https://a0.muscache.com/im/pictures/80d03de5-beac-46bf-bccc-6fd966631e83.jpg?im_w=1200",
        rating: "4.57",
        reviews: 7,
        title: "Ashirvad farm stay",
        description: "Ashirvad farm stay Farm stay hosted by Karthick",
        price: "5,929",
    },
    {
        src:
            "https://a0.muscache.com/im/pictures/1aa13ba4-c89d-40db-8913-65b34be39650.jpg?im_w=1200",
        rating: "4.87",
        reviews: 45,
        title: "RAI COTTAGE - Deluxe",
        description: "Farm stay hosted by Dr Shashikanth",
        price: "1,999",
    },
];
export default NearbyData;
