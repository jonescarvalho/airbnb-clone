import React from "react";
import styled from "styled-components";
import ImaginativeCard from "./ImaginativeCard";
import ImaginativeData from "./imaginativeCardDatas";
const ImaginativeStay = () => {
    return (
        <ImaginativeContainer>
            <ImaginativeTop>
                <Title>Imaginative stays close to home</Title>
                <Description>
                    Get inspired by your surroundings in these architectural
                    gems.
                </Description>
            </ImaginativeTop>
            <ImaginativeBottom>
                {ImaginativeData.map((data, index) => {
                    return (
                        <ImaginativeCard
                            src={data.src}
                            rating={data.rating}
                            reviews={data.reviews}
                            title={data.title}
                            description={data.description}
                            price={data.price}
                        />
                    );
                })}
            </ImaginativeBottom>
            <Showall>
                <u>Show all Imaginative Imaginative stays</u>
            </Showall>
        </ImaginativeContainer>
    );
};

export default ImaginativeStay;

const ImaginativeContainer = styled.div`
    // height: 0vh;
    padding: 0 100px 20px;
`;
const ImaginativeTop = styled.div`
    margin-bottom: 20px;
`;
const Title = styled.h2`
    font-size: 24px;
    font-weight: 600;
    // margin-bottom: 10px;
`;
const Description = styled.p``;
const ImaginativeBottom = styled.div`
    display: flex;
    // justify-contaent: flex-start;
    overflow-y: hidden;
    overflow-x: scroll;
    &::-webkit-scrollbar {
        display: none;
    }
`;
const Showall = styled.button`
    background: none;
    border: none;
    font-size: 15px;
    font-weight: 600;
    margin-top: 20px;
    cursor: pointer;
    padding: 10px;
    &:focus {
        outline: none;
    }
    &:hover {
        background-color: rgba(0, 0, 0, 0.04);
    }
`;
