const ImaginativeData = [
    {
        src:
            "https://a0.muscache.com/im/pictures/19167c7a-2456-44b6-8534-a521771dd3cb.jpg?im_w=1200",
        rating: "3.5",
        reviews: 2,
        title: "Tiny House-Bangalore ",
        description: "Tiny Farm House with BBQ, Bon fire, AC, Fridge etc",
        price: "5,457",
    },
    {
        src:
            "https://a0.muscache.com/im/pictures/bbe4dba9-a0ac-4f66-8035-6791e8d3fa70.jpg?im_w=1200",
        rating: "4.8",
        reviews: 98,
        title: "Tiny House-Pondicerry",
        description: "Tiny house hosted by Simon, Germain",
        price: "2,800",
    },
    {
        src:
            "https://a0.muscache.com/im/pictures/miso/Hosting-45430389/original/475b16b4-631c-49e0-8091-7e1aa0afe012.jpeg?im_w=1200",
        rating: "No Rating",
        reviews: 0,
        title: "Beach cottage",
        description: "Tiny house hosted by Sheril",
        price: "7,200",
    },
    {
        src:
            "https://a0.muscache.com/im/pictures/b0b97926-79bd-4dda-a45e-8cb58b9f6a11.jpg?im_w=1200",
        rating: "3.0",
        reviews: 1,
        title: "Ceylon Kite Ranch 2",
        description: "Berigai Hill Home. Great View, 2 beds, Food, Wifi",
        price: "3,999",
    },
    {
        src:
            "https://a0.muscache.com/im/pictures/9aaf8b2f-b276-4aca-9476-17e4b3681e01.jpg?im_w=1200",
        rating: "4.90",
        reviews: 118,
        title: "Mannoor Farm Stays",
        description: "Mannoor Farm Stays: Peaceful stay at the mountains",
        price: "2,499",
    },
    {
        src:
            "https://a0.muscache.com/im/pictures/15bee91a-3e3b-4827-8563-16799b339aaa.jpg?im_w=1200",
        rating: "4.80",
        reviews: 5,
        title: "Misty view Cottages",
        description: "Misty view Cottages on the hill. Coffee & Rain",
        price: "999",
    },
    {
        src:
            "https://a0.muscache.com/im/pictures/80d03de5-beac-46bf-bccc-6fd966631e83.jpg?im_w=1200",
        rating: "4.57",
        reviews: 7,
        title: "Ashirvad farm stay",
        description: "Ashirvad farm stay Farm stay hosted by Karthick",
        price: "5,929",
    },
    {
        src:
            "https://a0.muscache.com/im/pictures/1aa13ba4-c89d-40db-8913-65b34be39650.jpg?im_w=1200",
        rating: "4.87",
        reviews: 45,
        title: "RAI COTTAGE - Deluxe",
        description: "Farm stay hosted by Dr Shashikanth",
        price: "1,999",
    },
];
export default ImaginativeData;
