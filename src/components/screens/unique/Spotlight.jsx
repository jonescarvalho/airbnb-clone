import React from "react";
import styled from "styled-components";

const Spotlight = () => {
    return (
        <SpotlightContainer>
            <SpotlightContent>
                <Title>Unique stays</Title>
                <TitleInfo>
                    Tents, treehouses, and more – these spaces are more than
                    just a place to sleep.
                </TitleInfo>
            </SpotlightContent>
        </SpotlightContainer>
    );
};

export default Spotlight;
const SpotlightContainer = styled.div`
    background-image: url("https://a0.muscache.com/im/pictures/a4568b94-29ee-49cc-ab9c-5fcc82ca878d.jpg?im_w=1680");
    background-size: cover;
    background-repeat: no-repeat;
    // background-position: center center;
    height: calc(75vh - 70px);
    position: relative;
`;
const SpotlightContent = styled.div`
    color: #fff;
    position: absolute;
    left: 100px;
    bottom: 30vh;
`;
const Title = styled.h1`
    font-size: 55px;
    font-weight: 600;
`;
const TitleInfo = styled.h4`
    font-size: 18px;
    font-weight: 400;
    max-width: 400px;
    margin-bottom: 20px;
`;
