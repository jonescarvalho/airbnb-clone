import React, { useState } from 'react';
import ExploreSpotlight from './ExploreSpotlight';
import TopSpots from './TopSpots';
import styled from 'styled-components';
const ExploreHome = () => {
	const [places, setPlaces] = useState([
		{
			place: 'Munnar',
			pic:
				'https://a0.muscache.com/im/pictures/8dd3d770-1ea3-4f88-94a7-e2136c5315c3.jpg?aki_policy=large',
			link: '/unique/',
		},
		{
			place: 'Navi mumbai',
			pic:
				'https://a0.muscache.com/im/pictures/8d1f0498-69b4-4dca-8914-17eaf468c029.jpg?aki_policy=large',
			link: '/unique/',
		},
		{
			place: 'Rameshwaram',
			pic:
				'https://a0.muscache.com/im/pictures/765ac32b-9005-4fcc-bd75-a9b7751566ef.jpg?aki_policy=large',
			link: '/unique/',
		},
		{
			place: 'Kovalam',
			pic:
				'https://a0.muscache.com/im/pictures/2ee7d726-4da3-466d-899c-d3ed9f750052.jpg?aki_policy=large',
			link: '/',
		},
		{
			place: 'Goa',
			pic:
				'https://a0.muscache.com/im/pictures/db0734b6-2203-4937-b3c1-619b15c9c175.jpg?aki_policy=large',
			link: '/',
		},
		{
			place: 'Marine Drive',
			pic:
				'https://a0.muscache.com/im/pictures/80d09b8e-4d75-4f37-8c8f-c94c6ff45ac1.jpg?aki_policy=large',
			link: '/',
		},
		{
			place: 'Andheri',
			pic:
				'https://a0.muscache.com/im/pictures/e6c27983-c9b8-4f05-8887-73d7d64a1a0f.jpg?aki_policy=large',
			link: '/unique/',
		},
		{
			place: 'Humbi',
			pic:
				'https://a0.muscache.com/im/pictures/cb82d191-c1cf-496f-938c-b0589e443a86.jpg?aki_policy=large',
			link: '/',
		},
	]);
	return (
		<>
			<ExploreSpotlight />
			<TopSpotsContainer>
				{places.map((data, index) => {
					return <TopSpots data={data} />;
				})}
			</TopSpotsContainer>
		</>
	);
};

export default ExploreHome;
const TopSpotsContainer = styled.section`
	padding: 60px 100px;
	display: grid;
	grid-template-columns: 1fr 1fr 1fr 1fr;
	grid-row-gap: 30px;
	grid-column-gap: 10px;
`;
