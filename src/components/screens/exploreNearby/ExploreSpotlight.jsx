import styled from "styled-components";
import React from "react";

const Explore = () => {
    return (
        <ExploreConatainer>
            <ExploreInfo>
                <Title>Give your routine a new home</Title>
                <TitleInfo>
                    Settle in somewhere new. Discover nearby stays to live,
                    work, or just relax.
                </TitleInfo>
            </ExploreInfo>
        </ExploreConatainer>
    );
};

export default Explore;

const ExploreConatainer = styled.div`
    background-image: url("https://a0.muscache.com/pictures/18084f37-67e0-400f-bfd8-55eea0e89508.jpg");
    background-size: cover;
    background-repeat: no-repeat;
    // background-position: center center;
    min-height: 500px;
    height: calc(80vh - 70px);
    position: relative;
`;
const ExploreInfo = styled.div`
    color: #fff;
    position: absolute;
    left: 100px;
    bottom: 30vh;
`;
const Title = styled.h1`
    font-size: 55px;
    max-width: 400px;
    font-weight: 600;
    line-height: 60px;
`;
const TitleInfo = styled.h4`
    font-size: 18px;
    font-weight: 400;
    max-width: 400px;
    margin-bottom: 20px;
`;
