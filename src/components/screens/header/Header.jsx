import React from "react";
import Logo from "../../../asset/airbnb_logo.png";
import styled from "styled-components";
import SearchIcon from "@material-ui/icons/Search";
import LanguageIcon from "@material-ui/icons/Language";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Avatar from "@material-ui/core/Avatar";
import { Link } from "react-router-dom";

const Header = () => {
    return (
        <HeaderContainer>
            <LogoContainer>
                <LogoNav to="/">
                    <Picture src={Logo} alt="Airbnb logo" />
                </LogoNav>
            </LogoContainer>

            <SearchContainer>
                <FormContainer>
                    <InputSection
                        type="text"
                        // placeholder="search here...."
                    ></InputSection>
                    <SearchButton />
                </FormContainer>
            </SearchContainer>

            <UserContainer>
                <HostButton href="#">Become a host</HostButton>
                <languageButton>
                    <GlobalButton />
                    <DropDown />
                </languageButton>
                <User>
                    <Avatar />
                </User>
            </UserContainer>
        </HeaderContainer>
    );
};

export default Header;

const HeaderContainer = styled.div`
    position: sticky;
    top: 0;
    left: 0;
    z-index: 9999;
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 70px;
    padding: 10px 100px;
    border-bottom: 1px solid #e7e7e7;
    background-color: #fff;
`;
const LogoContainer = styled.h1`
    width: 100px;
`;
const LogoNav = styled(Link)`
    display: block;
`;
const Picture = styled.img`
    display: block;
    width: 100%;
`;
const SearchContainer = styled.form`
    flex: 1;
`;
const FormContainer = styled.form`
    display: flex;
    align-items: center;
    width: 400px;
    margin: 0 auto;
    border: 2px solid lightgrey;
    padding: 10px 20px;
    border-radius: 30px;
`;
const InputSection = styled.input`
    border: none;
    flex: 1;
    color: #595959;
    font-size: 18px;
    &:focus {
        outline: none;
    }
`;
const SearchButton = styled(SearchIcon)`
    color: #595959;
`;

const UserContainer = styled.div`
    display: flex;
    align-items: center;
    width: 250px;
    justify-content: space-between;
    height: 100px;
`;
const HostButton = styled.a`
    display: inline-block;
    font-weight: 400;
    color: #595959;
`;
const GlobalButton = styled(LanguageIcon)`
    font-size: 25px !important;
    color: #595959;
`;
const DropDown = styled(ExpandMoreIcon)`
    font-size: 25px !important;
    color: #595959;
`;
const languageButton = styled.div``;
const User = styled.div``;
