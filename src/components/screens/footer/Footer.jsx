import React from "react";
import styled from "styled-components";
import About from "./About.jsx";
import Comunity from "./Comunity.jsx";
import Host from "./Host.jsx";
import Support from "./Support.jsx";
import FooterBottom from "./FooterBottom.jsx";
const Links = [
    { links: "How Airbnb works" },
    { links: "Newsrooms" },
    { links: "Airbnb Plus" },
    { links: "Airbnb luke" },
    { links: "Hotel Tonight" },
    { links: "Airbnb for work" },
    { links: "Olympics" },
    { links: "Careers" },
];

const Footer = () => {
    return (
        <FooterContainer>
            <FooterTop>
                <ItemContainer>
                    <Title>About</Title>
                    {Links.map((data, index) => {
                        return <About links={data.links} />;
                    })}
                </ItemContainer>
                <ItemContainer>
                    <Title>Comunity</Title>
                    {Links.map((data, index) => {
                        return <Comunity links={data.links} />;
                    })}
                </ItemContainer>
                <ItemContainer>
                    <Title>Host</Title>
                    {Links.map((data, index) => {
                        return <Host links={data.links} />;
                    })}
                </ItemContainer>
                <ItemContainer>
                    <Title>Support</Title>
                    {Links.map((data, index) => {
                        return <Support links={data.links} />;
                    })}
                </ItemContainer>
            </FooterTop>
            <FooterBottom />
        </FooterContainer>
    );
};

export default Footer;

const FooterContainer = styled.section`
    background-color: #f3f3f3;
    margin-bottom: 10px;
`;
const FooterTop = styled.div`
    display: flex;
    justify-content: flex-start;
    border: 1px solid #cccccc;
    padding: 60px 100px !important;
`;
const ItemContainer = styled.div`
    width: 25%;
`;
const Title = styled.h2`
    font-size: 24px;
    font-weight: 600;
    margin-bottom: 30px;
`;
