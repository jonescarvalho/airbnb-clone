import React from "react";
import styled from "styled-components";
import LanguageIcon from "@material-ui/icons/Language";
import FacebookIcon from "@material-ui/icons/Facebook";
import TwitterIcon from "@material-ui/icons/Twitter";
import InstagramIcon from "@material-ui/icons/Instagram";

const FooterBottom = () => {
    return (
        <Bottom>
            <BottomLeft>
                <CopyRight>
                    © 2020 Airbnb, Inc. All rights reserved .{" "}
                </CopyRight>
                <CopyRightLinks> Privacy </CopyRightLinks>
                <CopyRight> . </CopyRight>
                <CopyRightLinks> Terms </CopyRightLinks>
                <CopyRight> . </CopyRight>
                <CopyRightLinks> Sitemap </CopyRightLinks>
            </BottomLeft>
            <BottomRight>
                <Button>
                    <LanguageIcon /> <u>English</u>
                </Button>
                <Button>
                    &#8377; <b> INR</b>
                </Button>
                <Icons>
                    <Link href="#">
                        <FacebookIcon />
                    </Link>
                    <Link href="#">
                        <TwitterIcon />
                    </Link>
                    <Link href="#">
                        <InstagramIcon />
                    </Link>
                </Icons>
            </BottomRight>
        </Bottom>
    );
};

export default FooterBottom;

const Bottom = styled.div`
    display: flex;
    justify-content: space-between;
    padding: 20px 100px;
`;
const BottomLeft = styled.div`
    display: flex;
    justify-content: flex-start;
`;
const CopyRight = styled.p`
    font-size: 15px;
`;
const CopyRightLinks = styled.a`
    font-size: 16px;
    cursor: pointer;
    padding: 0 5px;
    &:hover {
        border-bottom: 1px solid #000;
    }
`;
const BottomRight = styled.div`
    display: flex;
    justify-content: space-between;
`;
const Button = styled.button`
    display: flex;
    align-items: center;
    background: none;
    border: none;
    cursor: pointer;
    margin-right: 20px;
    &:focus {
        outline: none;
    }
`;
const Icons = styled.div`
    width: 120px;
    display: flex;
    justify-content: space-between;
`;
const Link = styled.a`
    display: block;
`;
