import React from "react";
import styled from "styled-components";

const Support = (props) => {
    return <Link href="#">{props.links} </Link>;
};

export default Support;
const Link = styled.a`
    font-size: 15px;
    display: block;
    // width: 100%;
    margin-bottom: 20px;
    cursor: pointer;
    &:hover {
        color: #73726f;
    }
`;
