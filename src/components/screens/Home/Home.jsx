import React from "react";
import styled from "styled-components";
import Banner from "./includes/Banner";
import Card from "./includes/Card";
import CardData from "./includes/cardData";
import Destination from "./includes/Destination";
import OnlineExperience from "./includes/OnlineExperience";

const Home = () => {
    return (
        <HomeContainer>
            <Banner />
            <CardContainer>
                {CardData.map((data, index) => {
                    return (
                        <Card
                            key={index}
                            src={data.src}
                            title={data.title}
                            description={data.description}
                        />
                    );
                })}
            </CardContainer>
            <OnlineExperience />
            <Destination />
        </HomeContainer>
    );
};

export default Home;

const HomeContainer = styled.div``;
const CardContainer = styled.div`
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
    padding: 30px 100px;
`;
