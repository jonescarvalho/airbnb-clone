import React, { useState } from "react";
import styled from "styled-components";
import Button from "@material-ui/core/Button";
import DateSearch from "./DateSearch";
import { Link } from "react-router-dom";

const Banner = () => {
    const [showSearch, setShowSearch] = useState(false);
    return (
        <BannerConatainer>
            <BannerSearch>
                {showSearch && <DateSearch />}
                <SearchButton onClick={() => setShowSearch(!showSearch)}>
                    {showSearch ? "hide" : "Search dates"}
                </SearchButton>
            </BannerSearch>
            <BannerInfo>
                <Title>Go Near</Title>
                <TitleInfo>
                    Settle in somewhere new. Discover nearby stays to live,
                    work, or just relax.
                </TitleInfo>
                <ButtonIcon to="/explore/">Explore nearby</ButtonIcon>
            </BannerInfo>
        </BannerConatainer>
    );
};

export default Banner;

const BannerConatainer = styled.div`
    background-image: url("https://a0.muscache.com/pictures/18084f37-67e0-400f-bfd8-55eea0e89508.jpg");
    background-size: cover;
    background-repeat: no-repeat;
    // background-position: center center;
    height: calc(100vh - 70px);
    position: relative;
`;
const BannerSearch = styled.div``;
const SearchButton = styled(Button)`
    background-color: #fff !important;
    outline: none;
    color: #f45960 !important;
    border: none;
    box-shadow: none;
    padding: 10px 30px;
    cursor: pointer;
    font-weight: 600 !important;
    width: 100%;
    font-size: 18px;
    transition: 0.2s;
    text-transform: inherit;
`;
const BannerInfo = styled.div`
    color: #fff;
    position: absolute;
    left: 100px;
    bottom: 30vh;
`;
const Title = styled.h1`
    font-size: 55px;
    font-weight: 600;
`;
const TitleInfo = styled.h4`
    font-size: 18px;
    font-weight: 400;
    max-width: 400px;
    margin-bottom: 20px;
`;
const ButtonIcon = styled(Link)`
    background-color: #fff !important;
    text-transform: inhert;
    font-weight: 600 !important;
    color: #000 !important;
    padding: 7px 10px;
    border: none;
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
    border-radius: none !important;
    &:hover {
        background-color: #000 !important;
        color: #fff !important;
    }
    &:focus {
        outline: none;
    }
`;
