import React from "react";
import styled from "styled-components";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import Gallery from "./Gallery";

const OnlineExperience = () => {
    return (
        <OnlineExperienceContainer>
            <TitleSection>
                <SectionLeft>
                    <Title>Online Experiences</Title>
                    <Description>
                        Meet people all over the world while trying something
                        new. Join live, interactive video sessions led by
                        one-of-a-kind hosts – all without leaving home.
                    </Description>
                </SectionLeft>
                <SectionRight>
                    <ExploreAllButtton>
                        <Explore to="">Explore All</Explore>
                    </ExploreAllButtton>
                </SectionRight>
            </TitleSection>
            <GallerySection>
                <Gallery />
            </GallerySection>
        </OnlineExperienceContainer>
    );
};

export default OnlineExperience;
const OnlineExperienceContainer = styled.section`
    width: 100%;
    background-color: #000;
    padding: 60px 100px;
`;
const TitleSection = styled.div`
    display: flex;
    justify-content: space-between;
    margin-bottom: 30px;
`;
const SectionLeft = styled.div``;
const Title = styled.h2`
    color: #fff;
    font-size: 32px;
    font-weight: 600;
    margin-bottom: 10px;
`;
const Description = styled.p`
    color: #fff;
    max-width: 500px;
`;
const SectionRight = styled.div``;

const ExploreAllButtton = styled(Button)`
    color: #fff !important;
    border: 1px solid #fff !important;
`;
const Explore = styled(Link)`
    display: block;
    color: #fff;
`;
const GallerySection = styled.div`
    // display: grid;
    // grid-template-columns: 2fr 1fr 1fr;
`;
