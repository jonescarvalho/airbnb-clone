import React, { useState } from "react";
import styled from "styled-components";
import PeopleIcon from "@material-ui/icons/People";

import "react-date-range/dist/styles.css";
import "react-date-range/dist/theme/default.css";
import { DateRangePicker } from "react-date-range";

const DateSearch = () => {
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());

    const selectionRange = {
        startDate: startDate,
        endDate: endDate,
        key: "selection",
    };

    const hangleSelect = (ranges) => {
        setStartDate(ranges.selection.startDate);
        setEndDate(ranges.selection.endDate);
    };

    return (
        <DateSearchContainer>
            <Calender ranges={[selectionRange]} onChange={hangleSelect} />
            <GuestNumber>
                Number of guests <Guests />
            </GuestNumber>
            <Count min={0} defaultValue={2} type="number" />
            <SearchButton>Search Airbnb</SearchButton>
        </DateSearchContainer>
    );
};

export default DateSearch;

const DateSearchContainer = styled.div`
    position: absolute;
    top: 55px;
    left: 25%;
    z-index: 99;
    transition: 0.5s;
`;
const Calender = styled(DateRangePicker)``;
const GuestNumber = styled.h2`
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 559px;
    padding: 10px;
    background-color: white;
    position: absolute;
    left: 0;
    top: 345px;
    font-weight: 300;
    font-size: 16px;
`;
const Guests = styled(PeopleIcon)`
    font-size: 20px;
`;
const Count = styled.input`
    width: 559px;
    padding: 20px;
    position: absolute;
    left: 0;
    top: 388px;
    border: none;
    -webkit-appearance: none;
`;
const SearchButton = styled.button`
    position: absolute;
    left: 0;
    top: 445px;
    text-transforn: inherit;
    background-color: #ff7779 !important;
    width: 559px;
    border: none;
    outline: none;
    padding: 15px;
    color: #fff !important;
    font-weight: 600;
    transition: 0.3s;
    &:hover {
        background-color: #fff !important;
        color: #ff7779 !important;
    }
`;
