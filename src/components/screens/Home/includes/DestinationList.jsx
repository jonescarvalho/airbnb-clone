import React from "react";
import styled from "styled-components";
const DestinationList = ({ destination }) => {
    return (
        <ListContainer>
            <City>{destination.city}</City>
            <State>{destination.state}</State>
        </ListContainer>
    );
};

export default DestinationList;
const ListContainer = styled.div`
    width: 25%;
    height: 70px;
`;
const City = styled.p``;
const State = styled.span`
    display: block;
    color: #717171;
`;
