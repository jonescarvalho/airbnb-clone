// import react from "react";

const cardData = [
    {
        src:
            "https://a0.muscache.com/im/pictures/15159c9c-9cf1-400e-b809-4e13f286fa38.jpg?im_w=720",
        title: "Unique stays",
        description: "spaces are more than just a place to sleep.",
        path: "/unique/",
    },
    {
        src:
            "https://a0.muscache.com/im/pictures/4a2f688e-0b33-4feb-932f-494b9a37348c.jpg?im_w=720",
        title: "Online Experiences",
        description:
            "Unique activities we can do together, led by a world of hosts.",
    },
    {
        src:
            "https://a0.muscache.com/im/pictures/fdb46962-10c1-45fc-a228-d0b055411448.jpg?im_w:720",
        title: "Entrie Homes",
        description:
            "Comfortable private places, with room for friends or family.",
    },
    {
        src:
            "https://a0.muscache.com/im/pictures/ea126ca5-6a02-4742-af1f-cb0e404180de.jpg?im_w:720",
        title: "Monthly Stays",
        description:
            "Settle in and experience life somewhere new, with cosy stays of a month or longer.",
    },
    {
        src:
            "https://a0.muscache.com/im/pictures/3c979022-9817-4b41-85e7-f586f5f3f277.jpg?im_w:720",
        title: "Work or learn from anywhere",
        description:
            "Swap your home office or classroom for a convenient stay with wifi.",
    },
    {
        src:
            "https://a0.muscache.com/im/pictures/47d3d81e-1c82-4bc6-928d-ecfaa4b9f456.jpg?im_w:720",
        title: "Family-friendly homes",
        description:
            "Give the family a break from the same four walls with these entire homes nearby.",
    },
];

export default cardData;
