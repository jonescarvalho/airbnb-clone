import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

const Card = (props) => {
    return (
        <CardContainer to="/unique/ ">
            <PictureContainer>
                <Picture src={props.src} alt="" />
            </PictureContainer>
            <TextContainer>
                <Title>{props.title}</Title>
                <Description>{props.description}</Description>
                <Price>{props.price}</Price>
            </TextContainer>
        </CardContainer>
    );
};

export default Card;

const CardContainer = styled(Link)`
    width: 32.33%;
    border-radius: 18px;
    overflow: hidden;
    box-shadow: rgba(0, 0, 0, 0.15) 0px 2px 8px !important;
    cursor: pointer;
    margin-bottom: 50px;
    display: block;
`;
const PictureContainer = styled.div``;
const Picture = styled.img`
    display: block;
    width: 100%;
`;
const TextContainer = styled.div`
    padding: 20px;
    background-color: #fff;
    border-radius: 16px;
`;
const Title = styled.h3`
    font-size: 18px;
    font-weight: 600;
`;
const Description = styled.h4`
    font-size: 15px;
    font-weight: 400;
    color: rgb(113, 113, 113) !important;
`;
const Price = styled.h6``;
