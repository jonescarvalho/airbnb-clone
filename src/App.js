import React from "react";
import "./App.css";
import { Route, Switch } from "react-router-dom";

import Home from "./components/screens/Home/Home";
import Header from "./components/screens/header/Header.jsx";
import Footer from "./components/screens/footer/Footer";
import Unique from "./components/screens/unique/Unique.jsx";
import ExploreHome from "./components/screens/exploreNearby/ExploreHome";

function App() {
    return (
        <div className="App">
            <Header />
            <Switch>
                <Route exact path="/" component={Home}></Route>
                <Route path="/unique/" component={Unique}></Route>
                <Route path="/explore/" component={ExploreHome}></Route>
            </Switch>
            <Footer />
        </div>
    );
}

export default App;
